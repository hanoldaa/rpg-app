﻿using UnityEngine;
using System.Collections;

public class MainMenuViewController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeInHierarchy)
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CampaignViewController : MonoBehaviour {

    public MainMenuViewController MainMenuView;
    public SignInViewController SignInView;

    public PostsController PostsController;

    public GameObject FullMessage;
    public InputField NewMessage;

    public List<Post> Posts = new List<Post>();

	// Use this for initialization
	void Start () {
        for (int i = 0; i < 10; i++ )
        {
            Post post = new Post(){
                ID = i+1,
                Character = Social.localUser.userName,
                Contents = "I introduce myself.",
                Date = System.DateTime.Now.ToString(),
                Tags = null
            };
            Posts.Add(post);
        }
        
        PostsController.Initialize(FullMessage, Posts);
        PostsController.GetComponent<ScrollRect>().verticalNormalizedPosition = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if(gameObject.activeInHierarchy)
	        if(Input.GetKeyDown(KeyCode.Escape))
            {
                MainMenuView.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
	}

    public void SubmitPost()
    {
        Post post = new Post()
        {
            ID = Posts.Count,
            Character = Social.localUser.userName,
            Contents = NewMessage.text,
            Date = System.DateTime.Now.ToString(),
            Tags = null
        };

        Posts.Add(post);

        PostsController.Initialize(FullMessage, Posts);
    }

    public void NewPostClicked()
    {
        NewMessage.text = "";
        NewMessage.textComponent.text = "";
        NewMessage.gameObject.SetActive(true);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PostController : MonoBehaviour {

    public int ID { get; set; }

    public Image Avatar;
    public Text Message;
    public Text PlayerName;
    private GameObject FullMessage;

    Post Post { get; set; }
    
    public void Start()
    {
    }
    public void Initialize(GameObject fullMessage, Post post)
    {
        FullMessage = fullMessage;
        Post = post;
        PlayerName.text = Post.Character;
        Message.text = Post.Contents;
    }

    public void ToggleFullMessage()
    {
        FullMessage.SetActive(true);
        FullMessage.GetComponentInChildren<Text>().text = Message.text;
    }
}

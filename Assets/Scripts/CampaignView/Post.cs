﻿using System.Collections;
using System.Collections.Generic;

public class Post{

    public int ID { get; set; }

    public string User { get; set; }
    public string Character { get; set; }
    public string Contents { get; set; }
    public string Date { get; set; }
    public List<string> Tags { get; set; }
}

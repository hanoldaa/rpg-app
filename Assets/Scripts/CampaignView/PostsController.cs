﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PostsController : MonoBehaviour {

    public List<Post> Posts;
    public List<PostController> PostControllers;

    public PostController PostControllerPrefab;
    public GameObject Contents;

    public void Initialize(GameObject fullMessage, List<Post> posts)
    {
        Posts = posts;
        
        foreach(var child in Contents.GetComponentsInChildren<PostController>())
        {
            Destroy(child.gameObject);
        }

        foreach(var post in Posts)
        {
            PostController postController = (PostController)Instantiate(PostControllerPrefab);
            PostControllers.Add(postController);

            postController.transform.parent = Contents.transform;
            postController.Initialize(fullMessage, post);
        }

        StartCoroutine(ScrollToBottom());
    }

    IEnumerator ScrollToBottom()
    {
        yield return null;

        GetComponent<ScrollRect>().verticalNormalizedPosition = 0;

        StopCoroutine(ScrollToBottom());
    }
}

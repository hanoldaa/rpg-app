﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GooglePlay : MonoBehaviour {

    //public SignInViewController SignInView;
    public GameObject MainMenuView;
    public CampaignViewController CampaignView;

	// Use this for initialization
	void Start () {
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();


        // authenticate user:
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Sign In Successful.");
                //SignInView.gameObject.SetActive(false);
            }
            else
            {
                Debug.Log("Sign In Unsuccessful.");
            }
        });
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SignIn()
    {
        // authenticate user:
        Social.localUser.Authenticate((bool success) =>
        {
            if(success)
            {
                Debug.Log("Sign In Successful.");
                //SignInView.gameObject.SetActive(false);
            }
            else
            {
                Debug.Log("Sign In Unsuccessful.");
            }
        });
    }

    public void SignOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();

        SignIn();
    }

    public void AchievementAdd()
    {
        Social.ReportProgress("CgkImID7454LEAIQAQ", 100.0f, (bool success) => {
            
        });
    }

    public void AchievementShow()
    {
        Social.ShowAchievementsUI();
    }

    string Leaderboard = "CgkImID7454LEAIQBg ";
    public void LeaderboardAddPoints()
    {
        Social.ReportScore(500, Leaderboard, (bool success) =>
        {

        });
    }

    public void LeaderboardShow()
    {
        Social.ShowLeaderboardUI();
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}

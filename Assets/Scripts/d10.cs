﻿using UnityEngine;
using System.Collections;

public class d10 : MonoBehaviour {

	public class DiceEnum : MonoBehaviour
	{
		public enum DiceSide
		{
			one = 1,
			two = 2,
			three = 3,
			four = 4,
			five = 5,
			six = 6,
			seven = 7,
			eight = 8,
			nine = 9,
			ten = 10
		}
	}
	
	DiceEnum.DiceSide currentSide;
	bool Rolling = false; 

	private Vector3 GetDieRotation(DiceEnum.DiceSide diceSide)
	{
		switch (diceSide)
		{
			case DiceEnum.DiceSide.one:
				return new Vector3(-110, 0, -215);
				break;

			case DiceEnum.DiceSide.two:
				return new Vector3(70, 0, -145);
				break;

			case DiceEnum.DiceSide.three:
				return new Vector3(-110, 0, -72);
				break;

			case DiceEnum.DiceSide.four:
				return new Vector3(70, 0, 0);
				break;

			case DiceEnum.DiceSide.five:
				return new Vector3(-110, 0, 0);
				break;

			case DiceEnum.DiceSide.six:
				return new Vector3(70, 0, -70);
				break;

			case DiceEnum.DiceSide.seven:
				return new Vector3(-110, 0, -145);
				break;

			case DiceEnum.DiceSide.eight:
				return new Vector3(70, 0, -215);
				break;

			case DiceEnum.DiceSide.nine:
				return new Vector3(-110, 0, -290);
				break;

			case DiceEnum.DiceSide.ten:
				return new Vector3(70, 0, -288);
				break;
		}
		return new Vector3(0, 0, 0);
	}

	public void RandomSpin(DiceEnum.DiceSide newDiceSide)
	{
		// new vector + multiples of 360
		currentSide = newDiceSide;
		LeanTween.rotate(gameObject, GetDieRotation(newDiceSide) + new Vector3(1440, 1440, 2880), 1.0f);
	}
	public void Roll()
	{
		Vector3 cameraPos = Camera.main.transform.position;
		transform.position = new Vector3 (cameraPos.x, cameraPos.y, 0);

		gameObject.SetActive(true);

		if(!Rolling)
		{
			Rolling = true;

			RandomSpin((DiceEnum.DiceSide)Random.Range(1, 10));
			StartCoroutine(Hide());
		}
	}

	IEnumerator Hide()
	{
		yield return new WaitForSeconds(2.0f);
		gameObject.SetActive(false);

		Rolling = false;
		StopCoroutine(Hide());
	}

	void Update()
	{
		if(gameObject.activeInHierarchy)
		{
			Vector3 cameraPos = Camera.main.transform.position;
			transform.position = new Vector3 (cameraPos.x, cameraPos.y, 0);
		}
	}
}
